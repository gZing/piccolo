===========
  Piccolo
===========

A web app to capture your life events into one picture a day.
Live app: http://piccolos.herokuapp.com

Installation
-------------
To run this project follow these steps:

|    $ git clone https://gZing@bitbucket.org/gZing/piccolo.git
|    $ cd piccolo

Assuming you have Python 2.7, VirtualEnvWrapper installed,
    
|    #. Create your working environment
|    $ mkvirtualenv piccolo
|    $ workon piccolo
|
|    #. Install dependencies
|    $ pip install -r requirements/local.txt
|
|    #. Run the app
|    $ python manage.py runserver
