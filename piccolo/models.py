import datetime
import os
from django.db import models
from django.contrib.auth.models import User


def get_file_path(instance, filename):
		pub_date = datetime.datetime.now()
		ext = filename.split('.')[-1]
		filename = "%s.%s" % ( pub_date.strftime('%m-%d-%Y'), ext)
		return os.path.join("piccolo/" + instance.user.username + "/", filename) 

class Piccolo(models.Model):
	user = models.ForeignKey(User, related_name='piccolo')
	pic_url = models.FileField(upload_to=get_file_path , default = '/media/None/no-img-post.jpg')
	text = models.CharField(max_length=140)
	pub_date = models.DateTimeField(auto_now_add=True)


	def __unicode__(self):
		return self.user.username


class Profile(models.Model):
	user = models.OneToOneField(User, related_name='profile')
	pic_file = models.FileField(upload_to='profile', default = 'None/no-img-post.jpg')
	bio = models.CharField(max_length=300,null=True)

	def __unicode__(self):
		return self.user.username