import datetime
from django.shortcuts import render, redirect
from django.contrib.auth import logout, login, authenticate
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from piccolo.forms import *
from piccolo.models import *


def home(request):
    if request.user.is_authenticated():
        return redirect("timeline")
    
    else:
        return render(request, 'index.html')


def signup(request):
    
    form = SignupForm()
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data["username"],
                                            password=form.cleaned_data["password"],
                                            email=form.cleaned_data["email"])
            Profile(user=user).save()
            user = authenticate(username=form.cleaned_data["username"],
                                password=form.cleaned_data["password"])
            login(request, user)

            return redirect("home")

    return render(request, "signup.html", {"form": form})


def logins(request):
    form = LoginForm()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            print form.user
            login(request, form.user)
            return redirect("home")
    return render(request, "login.html", {"form": form})


def logouts(request):
    if request.user:
        logout(request)
    return redirect("home")
    
@login_required
def profile(request):
    return render(request, "profile.html")

@login_required
def edit(request):
    form = ProfilePicForm()
    if request.method == "POST":
        form = ProfilePicForm(request.POST, request.FILES)
        print form.is_valid()
        print " hey +++++++++++++++++++++++ lookk"
        if form.is_valid():
            profile = Profile.objects.get(user=request.user)
            if form.cleaned_data.get('bio', False):
                profile.bio = form.cleaned_data["bio"]

            if request.FILES.get('profile_pic', False):
                profile.pic_file = request.FILES['profile_pic']
            profile.save()

    return render(request, "edit.html")

@login_required
def newPost(request):
    form = newPostForm(request=request)
    date = datetime.datetime.now()
    print "+++++++++++++++++++++++++++++"
    if request.method == "POST":
        form = newPostForm(request.POST, request.FILES,request=request)
        if form.is_valid():
            print "========= still into u ========"

            piccolo = Piccolo(user=request.user, pic_url=request.FILES['image'], text=form.cleaned_data['text'])
            piccolo.save()

    return render(request, "app.html", { "form": form, 'date':date })

@login_required
def timeline(request):
    piccolos_list = Piccolo.objects.filter(user = request.user).order_by('-pub_date')
    p = Paginator(piccolos_list, 10)

    page = request.GET.get('page')
    try:
        piccolos = p.page(page)
    except PageNotAnInteger:
        piccolos = p.page(1)
    except EmptyPage:
        piccolos = p.page(p.num_pages)
    
        
    return render(request, "timeline.html", {'piccolos':piccolos})