from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from piccolo.models import *

import datetime


class SignupForm(forms.Form):
    username = forms.CharField(max_length=50, required=True)
    password = forms.CharField(min_length=8, required=True, widget=forms.PasswordInput())
    password2 = forms.CharField(required=True, widget=forms.PasswordInput())
    email = forms.EmailField(required=True)

    def clean(self):
        data = super(SignupForm, self).clean()
        if not self.errors:
            try:
                user = User.objects.get(Q(username=data["username"]) | Q(email=data["email"]))
                if user.username == data["username"]:
                    raise forms.ValidationError("Username already in use")
                if user.email == data["email"]:
                    raise forms.ValidationError("Email address already in use")
            except User.DoesNotExist:
                pass

            if data["password"] != data["password2"]:
                raise forms.ValidationError("Passwords don't match")

        return data


class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, required=True)
    password = forms.CharField(required=True, widget=forms.PasswordInput())


    def clean(self):
        cleaned_data = super(LoginForm, self).clean()
        self.user = authenticate(username=cleaned_data.get("username"),
                                 password=cleaned_data.get("password"))

        if not self.user:
            raise forms.ValidationError("Username or Password incorrect")

        return cleaned_data


class ProfilePicForm(forms.Form):
    profile_pic = forms.FileField(required=False)
    bio = forms.CharField(max_length=500, required=False)
    

class newPostForm(forms.Form):
    image = forms.FileField()
    text = forms.CharField(max_length=140, required=False)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request")
        super(newPostForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(newPostForm, self).clean()

        if not self.errors:
            try:
                today = datetime.datetime.today()
                post = Piccolo.objects.filter(user=self.request.user, pub_date__year=today.year, pub_date__month=today.month, pub_date__day=today.day)
                if post:
                    raise forms.ValidationError("Only one post a day is allowed!!")
                
            except ObjectDoesNotExist:
                pass

        return cleaned_data
