from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
admin.autodiscover()

urlpatterns = patterns('', 
    # Examples:
    url(r'^$', 'piccolo.views.home', name='home'),
    url(r'^signup/', 'piccolo.views.signup', name='signup'),
    url(r'^post/', 'piccolo.views.newPost', name='newPost'),
    url(r'^login/', 'piccolo.views.logins', name='login'),
    url(r'^profile/$', 'piccolo.views.profile', name='profile'),
    url(r'^profile/edit/', 'piccolo.views.edit', name='edit'),
    url(r'^timeline/', 'piccolo.views.timeline', name='timeline'),
    url(r'^logout/', 'django.contrib.auth.views.logout', {'next_page': '/'}, name='logout'),
    url(r'^admin/', include(admin.site.urls))
) 

if settings.DEBUG:
    from django.views.static import serve
    _media_url = settings.MEDIA_URL
    if _media_url.startswith('/'):
        _media_url = _media_url[1:]
        urlpatterns += patterns('',
                                (r'^%s(?P<path>.*)$' % _media_url,
                                serve,
                                {'document_root': settings.MEDIA_ROOT}))
    del(_media_url, serve)